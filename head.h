#ifndef HEADER_H
#define HEADER_H
#include<Windows.h>
typedef struct body
{
	COORD coord;
}BODY;

typedef struct player
{
	char nickname[20];
	char password[10];
	int BestScore1;
	int BestScore2;
}PLAYER;
void SnakePrint(BODY*, int);
void LastElementDelete(BODY*);
void FirstElementPrint(BODY*);
void MovingForward(BODY*, int);
void Direction(BODY*, char, char*);
void InitialSnake(BODY*, int);
int GameOver(BODY* , int );
void border();
BODY food(int, BODY*);
int Eating(BODY , BODY,int* );
void CreateGameFile();
void CreatePlayer();
int ExistigPlayer(PLAYER* player);
int PasswordCheck(char*);
void menu();
void PlayersBestScore(PLAYER*,int);
void ScoreTable(PLAYER**);
void zamjena(PLAYER* const, PLAYER* const);
void bubbleSort(PLAYER polje[], const int);
void ScoreTablePrint(PLAYER*, int);
int NumberOfPlayer();
#endif 
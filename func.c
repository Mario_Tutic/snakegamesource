#define _CRT_SECURE_NO_WARNINGS
#include"head.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>


void SnakePrint(BODY* snake, int ElementsNumber)
{
	int i;
	for (i = 0; i < ElementsNumber; i++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), (snake +i)->coord);
		printf("*");
	}
}

void LastElementDelete(BODY* LastElement)
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), LastElement->coord);
	printf(" ");
}

void FirstElementPrint(BODY* FirstElement)
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), FirstElement->coord);
	printf("*");
}

void MovingForward(BODY* snake, int ElementsNumber)
{
	ElementsNumber -= 1;
	for (; ElementsNumber > 0; ElementsNumber--)
	{
		(snake + ElementsNumber)->coord.X = (snake + (ElementsNumber - 1))->coord.X;
		(snake + ElementsNumber)->coord.Y = (snake + (ElementsNumber - 1))->coord.Y;
	}
}

void Direction(BODY *FirstElement,char NewDirection,char* OldDirection)
{
	if (!(NewDirection == '8' || NewDirection == '2' || NewDirection == '4' || NewDirection == '6'))
	{
		NewDirection = *OldDirection;
	}
	*OldDirection = NewDirection;
	switch (NewDirection)
	{
	case'8':
		FirstElement->coord.Y--;
		break;
	case'2':
		FirstElement->coord.Y++;
		break;
	case'4':
		FirstElement->coord.X--;
		break;
	case'6':
		FirstElement->coord.X++;
		break;
	}
}

void InitialSnake(BODY* snake,int ElementsNumber)
{
	int i;
	snake->coord.X = 50;
	snake->coord.Y = 12;
	for (i=1; i<ElementsNumber; i++)
	{
		(snake + i)->coord.X = (snake + (i - 1))->coord.X-1;
		(snake + i)->coord.Y = (snake + (i - 1))->coord.Y;
	}
}

int GameOver(BODY* snake,int ElementsNumber)
{
	COORD position;
	position.X = 13;
	position.Y = 12;
	int i;
	for (i = 1; i < ElementsNumber; i++)
	{
		if (snake->coord.Y == (snake + i)->coord.Y && snake->coord.X == (snake + i)->coord.X)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
			printf("GAME OVER...  ");
			printf("Press any key to return to the menu:");
			_getch();
			return -1;
		}
	}
	if (snake->coord.Y == 0 || snake->coord.Y == 24 || snake->coord.X == 0 || snake->coord.X == 70)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		printf("GAME OVER...  ");
		printf("Press any key to return to the menu:");
		_getch();
		return -1;
	}
	return 0;
}

void border()
{
	COORD position;
	position.Y = 0;
	for (position.X = 0; position.X < 70; position.X++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),position );
		printf("#");
	}
	position.Y = 24;
	for (position.X = 0; position.X < 71; position.X++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		printf("#");
	}


	position.X = 0;
	for (position.Y = 0; position.Y < 24; position.Y++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		printf("#");
	}
	position.X = 70;
	for (position.Y = 0; position.Y < 24; position.Y++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		printf("#");
	}
}


BODY food(int ElementsNumber, BODY* snake)
{
	int i,status=0;
	BODY FoodCoord;
	FoodCoord.coord.X = ((float)rand() / RAND_MAX) * 68 + 1;
	while (1)
	{
		for (i = 0; i < ElementsNumber; i++)
		{
			if (FoodCoord.coord.X == (snake + i)->coord.X)
			{
				status = 1;
				break;
			}
		}
		if (status == 0)
		{
			break;
		}
		else
		{
			FoodCoord.coord.X = rand() / RAND_MAX * 68 + 1;
			status = 0;
		}
	}
	FoodCoord.coord.Y = ((float)rand() / RAND_MAX) * 22 + 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), FoodCoord.coord);
	printf("F");
	return FoodCoord;
}

int Eating(BODY FoodCoord, BODY FirstElement,int* ElementsNumber)
{
	if ((FoodCoord.coord.X == FirstElement.coord.X) && (FoodCoord.coord.Y == FirstElement.coord.Y))
	{
		(*ElementsNumber)++;
		return 1;
	}
	else
		return 0;
}

void CreateGameFile()
{
	int zero = 0;
	FILE* fp;
	fp=fopen("GameFile.bin", "rb");
	if (fp == NULL)
	{
		fp = fopen("GameFile.bin","wb");
		if (fp == NULL)
		{
			printf("\nCreating GameFile fail!");
			return;
		}
		else
		{
			fwrite(&zero, sizeof(int), 1, fp);
			fwrite(&zero, sizeof(int), 1, fp);
			fwrite(&zero, sizeof(int), 1, fp);
		}
	}
	fclose(fp);
}

void CreatePlayer()
{
	int AccountsCounter;
	FILE* fp;
	PLAYER NewPlayer;
	fp = fopen("GameFile.bin", "rb+");
	if (fp == NULL)
	{
		printf("\nError while creating new player");
		return;
	}
	else
	{
		system("cls");
		printf("\nCreating new player, enter nickname:");
		fgets(&(NewPlayer.nickname), 20, stdin);
		printf("\nEnter password(maximum 10 characters):");
		fgets(&(NewPlayer.password), 10, stdin);
		NewPlayer.BestScore1 = 0;
		NewPlayer.BestScore2 = 0;
		fread(&AccountsCounter, sizeof(int), 1, fp);
		AccountsCounter++;
		rewind(fp);
		fwrite(&AccountsCounter, sizeof(int), 1, fp);
		fseek(fp, 0, SEEK_END);
		fwrite(&NewPlayer, sizeof(NewPlayer), 1, fp);
		fclose(fp);
		return;
	}
}

int ExistigPlayer(PLAYER* player)
{
	int n,i;
	char nickname[20], option;
	PLAYER ExistingPlayer;
	FILE* fp;
	fp=fopen("GameFile.bin", "rb");
	if (fp == 0)
	{
		printf("\nReading existing player data error");
		return -1;
	}
	else
	{
		fread(&n, sizeof(int), 1, fp);
		while(1)
		{
			system("cls");
			printf("\nEnter nickname:");
			fgets(nickname,20,stdin);
			fseek(fp, 3 * (sizeof(int)), SEEK_SET);
			for (i = 0; i < n; i++)
			{
				fread(&ExistingPlayer, sizeof(PLAYER), 1, fp);
				if (strcmp(ExistingPlayer.nickname, nickname) == 0)
				{
					*player = ExistingPlayer;
					fclose(fp);
					return 0;
				}
			}
			printf("\nPlayer with that nickname doesnt exist.");
			printf("\nChose:\n1. to enter name again\n2. to go back on game menu");
			printf("\nEnter ur option:");
			option = getche();
			while (1)
			{
				if (option=='1' || option == '2')
				{
					break;
				}
				system("cls");
				printf("\nThe entry is incorrect! Do it again.");
				printf("\nChose:\n1. to enter name again\n2. to go back on game menu");
				option = getche();
			}
			if (option == '2')
			{
				fclose(fp);
				return -1;
			}
		}
	}
}

int PasswordCheck(char*  RealPassword)
{
	char password[10],option;
	while (1)
	{
		system("cls");
		printf("\nEnter password to log in:");
		fgets(password, 10, stdin);
		if (strcmp(password, RealPassword)==0)
		{
			return 0;
		}
		else
		{
			printf("\nPassword is wrong.");
			while (1)
			{
				printf("\nChose:\n1. to enter password again\n2. to go back on game menu");
				printf("\nEnter option u want:");
				option = getche();
				if (option == '1' || option == '2')
				{
					break;
				}
				system("cls");
				printf("\nThe entry is incorrect! Do it again.");
			}
			if (option == '2')
			{
				return -1;
			}
		}
	}
}

void menu()
{
	printf("\nTo play game u have to sign in");
	printf("\nChose number in front of option u want to perform:");
	printf("\n1. Sign up:");
	printf("\n2. Create player:");
	printf("\n3. Play game");
	printf("\nEnter ur option:");
}


void PlayersBestScore(PLAYER *CurrentPlayer,int score)//ako je best score postignut updatea ga u binarnoj datoteci(prepisuje)
{
	PLAYER temporary;
	FILE *fp=NULL;
	if (CurrentPlayer->BestScore1 < score)
	{
		CurrentPlayer->BestScore1 = score;
		fp = fopen("GameFile.bin", "rb+");
		if (fp == NULL)
		{
			printf("\nError occured while writing best score");
			return;
		}
		else
		{
			fseek(fp, 3 * (sizeof(int)), SEEK_SET);
			while (1)
			{
				fread(&temporary, sizeof(PLAYER), 1, fp);
				if (strcmp(temporary.nickname, CurrentPlayer->nickname) == 0)
				{
					fseek(fp, -40, SEEK_CUR);
					fwrite(CurrentPlayer, sizeof(PLAYER), 1, fp);
					fclose(fp);
					return;
				}
			}
		}
	}
}

void ScoreTable(PLAYER**ListOfPlayers)//ucitavanje podataka svih igraca iz binarne datoteke u program 
{
	free(*ListOfPlayers);
	FILE* fp;
	int n,i;
	fp = fopen("GameFile.bin", "rb+");
	if (fp == NULL)
	{
		printf("\nError occured in ScoreTabel funcition while reading GameFile");
		return;
	}
	else
	{
		fread(&n, sizeof(int), 1, fp);
		*ListOfPlayers=(PLAYER*)calloc(n,sizeof(PLAYER));
		if (*ListOfPlayers == NULL)
		{
			printf("\nFail durig memory allocation");
			return;
		}
		else
		{
			fseek(fp, sizeof(int) * 2, SEEK_CUR);
			for (i = 0; i < n; i++)
			{
				fread(*ListOfPlayers + i, sizeof(PLAYER), 1, fp);
			}
			fclose(fp);
			return;
		}
	}
	
}

void zamjena(PLAYER* const veci, PLAYER* const manji)
{
	PLAYER temp;
	temp = *manji;
	*manji = *veci;
	*veci = temp;
}
void bubbleSort(PLAYER polje[], const int n) 
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (polje[j + 1].BestScore1> polje[j].BestScore1) {
				zamjena(&polje[j + 1], &polje[j]);
			}
		}
	}
}

void ScoreTablePrint(PLAYER* list, int n)
{
	int i;
	COORD NamePosition,ScorePosition;
	NamePosition.X = 75;
	NamePosition.Y = 0;
	ScorePosition.X = 95;
	ScorePosition.Y = 0;
	for (i = 0; i < n;i++)
	{
		NamePosition.Y += 1;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), NamePosition);
		printf("%d. %s",i+1,(list+i)->nickname);
		ScorePosition.Y += 1;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), ScorePosition);
		printf("%d", (list + i)->BestScore1);
	}
}

int NumberOfPlayer()
{
	FILE* fp;
	int n;
	fp = fopen("GameFile.bin", "rb+");
	if (fp == NULL)
	{
		printf("\nWarning! NumberOfPlayers function failed!");
		return;
	}
	else
	{
		fread(&n, sizeof(int),1, fp);
		fclose(fp);
		return n;
	}
}
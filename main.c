#define _CRT_SECURE_NO_WARNINGS
#include"head.h"
#include<stdio.h>
#include<stdio.h>
#include<time.h>
#include<string.h>


int main()
{
	srand(time(NULL));
	PLAYER CurrentPlayer,*list=NULL;
	BODY snake[50],FoodCoord;
	int ElementsNumber = 5,GameOverState=0, EatingState =0,score,PasState=-1,n;
	char NewDirection='6', OldDirection='6',option;
	CreateGameFile();
	ScoreTable(&list);
	n = NumberOfPlayer();
	bubbleSort(list, n);
	while (1)
	{
		system("cls");
		if (PasState == 0)
		{
			printf("\nYou are logged in you can play game now");
		}
		menu();
		ScoreTablePrint(list, n);
		option = getche();
		switch (option)
		{
		case'1':
			PasState = -1;
			if (ExistigPlayer(&CurrentPlayer) != -1)
			{
				PasState = PasswordCheck(CurrentPlayer.password);
				printf("\nPassword is correct you are logged in now\nPress any key to return to the menu:");
				_getch();
			}
			break;
		case'2':
			CreatePlayer();
			ScoreTable(&list);
			n = NumberOfPlayer();
			bubbleSort(list, n);
			printf("\nNew Player is created\nPress any key to return to the menu:");
			_getch();
			break;
		case'3':
			if (PasState == 0)
			{
				system("cls");
				ScoreTablePrint(list, n);
				while (1)
				{
					//snake game
					InitialSnake(snake, ElementsNumber);
					border();
					FoodCoord = food(ElementsNumber, snake);
					while (1)
					{
						if (_kbhit())
						{
							NewDirection = _getch();
						}
						LastElementDelete((snake + ElementsNumber - 1));
						MovingForward(snake, ElementsNumber);
						Direction(snake, NewDirection, &OldDirection);
						FirstElementPrint(snake);
						EatingState = Eating(FoodCoord, *snake, &ElementsNumber);
						if (EatingState == 1)
						{
							FoodCoord = food(ElementsNumber, snake);
							EatingState = 0;
						}
						if (GameOver(snake, ElementsNumber) == -1)
						{
							score = ElementsNumber - 5;
							PlayersBestScore(&CurrentPlayer, score);
							ElementsNumber = 5;
							ScoreTable(&list);
							bubbleSort(list, n);
							break;
						}
						Sleep(100);
					}
					break;
				}
			}
			break;
		}
	}
	return 0;
}